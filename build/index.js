'use strict';

exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = nest;

var _gw2eRecipeCalculation = require('gw2e-recipe-calculation');

var vendorItems = Object.keys(_gw2eRecipeCalculation.staticItems.vendorItems).map(function (x) {
  return parseInt(x, 10);
});

function nest(recipes) {
  var decorations = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  recipes = recipes.map(transformRecipe);

  // Ignore recipes that result in items that can be bought from vendors
  // This removes issues with wrong quantities in resulting recipes and
  // the wrong items (= container items) showing up in the shopping list
  recipes = recipes.filter(function (r) {
    return vendorItems.indexOf(r.id) === -1;
  });

  // Transform the array into a id maps to eliminate "find" calls, which can be very
  // slow if called on a big array (since they have to be called on every nesting)
  var recipesMapped = idMap(recipes);
  var recipeUpgrades = upgradeMap(recipesMapped);

  // Nest all recipes
  for (var i = 0; i < recipes.length; i++) {
    recipes[i] = nestRecipe(recipes[i], recipesMapped, recipeUpgrades, decorations);
  }

  // Remove the internal flag for nested recipes
  for (var _i = 0; _i < recipes.length; _i++) {
    delete recipes[_i]['nested'];
  }

  return Object.values(recipes);
}

function idMap(recipes) {
  var recipeMap = {};

  recipes.map(function (recipe) {
    recipeMap[recipe.id] = recipe;
  });

  return recipeMap;
}

function upgradeMap(recipeMap) {
  var recipeUpgrades = {};

  for (var key in recipeMap) {
    var upgradeId = recipeMap[key].upgrade_id;

    if (!upgradeId) {
      continue;
    }

    recipeUpgrades[upgradeId] = key;
  }

  return recipeUpgrades;
}

function transformRecipe(recipe) {
  var components = recipe.ingredients.map(function (i) {
    return {
      id: i.item_id,
      quantity: i.count
    };
  });

  if (recipe.guild_ingredients) {
    var guildIngredients = recipe.guild_ingredients.map(function (i) {
      return {
        id: i.upgrade_id,
        quantity: i.count,
        guild: true
      };
    });
    components = components.concat(guildIngredients);
  }

  var transformed = {
    id: recipe.output_item_id,
    recipeID: recipe.id !== undefined ? recipe.id : null,
    output: recipe.output_item_count,
    components: components,
    min_rating: recipe.min_rating !== undefined ? recipe.min_rating : null,
    disciplines: recipe.disciplines || []
  };

  if (recipe.output_upgrade_id) {
    transformed.upgrade_id = recipe.output_upgrade_id;
  }

  if (recipe.output_item_count_range) {
    transformed.output_range = recipe.output_item_count_range;
  }

  if (recipe.achievement_id) {
    transformed.achievement_id = recipe.achievement_id;
  }

  return transformed;
}

function nestRecipe(recipe, recipes, recipeUpgrades, decorations) {
  // This recipe was already nested as a part of another recipe
  if (recipe.nested) {
    return recipe;
  }

  // Calculate this recipe and all sub-components
  recipe.nested = true;
  recipe.quantity = recipe.quantity || 1;
  recipe.components = recipe.components.map(function (component) {
    var index = !component.guild ? component.id : recipeUpgrades[component.id];

    // We could not find a recipe for a normal component, so
    // we just give it back (e.g. basic woods)
    if (!component.guild && !recipes[index]) {
      return component;
    }

    // If it is a guild component and we can't find a recipe for it,
    // check if we can resolve it into an item, else ignore it
    if (component.guild && !recipes[index]) {
      return decorations[component.id] ? { id: decorations[component.id], quantity: component.quantity } : false;
    }

    // The component is the recipe! Abort! D:
    if (recipe.id === index) {
      return !component.guild ? component : { id: recipe.id, quantity: component.quantity };
    }

    // The component recipe is not nested yet, so we nest it now!
    if (!recipes[index].nested) {
      recipes[index] = nestRecipe(recipes[index], recipes, recipeUpgrades, decorations);
    }

    // Make sure we use a copy of the object, and insert it into the components
    var ingredientRecipe = _extends({}, recipes[index]);
    ingredientRecipe.quantity = component.quantity;
    delete ingredientRecipe.nested;

    return ingredientRecipe;
  });

  // Filter guild components that we don't have in our recipes :(
  recipe.components = recipe.components.filter(function (x) {
    return x;
  });

  // Sort components so that non-craftable components are always on top
  recipe.components.sort(function (a, b) {
    return (a.components ? 1 : 0) - (b.components ? 1 : 0);
  });

  // Throw out the components if they are empty (= only non-matched guild recipes)
  if (recipe.components.length === 0) {
    delete recipe.components;
  }

  return recipe;
}